# microservice

Ordermanagement Microservices
# Features 
1. Microservice
2. Swagger API doc(https://github.com/swagger-api/swagger-core/wiki/annotations)
3. Eureka as Discovery
4. Zuul as API gateway
5. Hystrix
6. Saga patteren transaction
7. Logging
8. Authentication at API gateway using JWT token
9. Feign client
10. Ribbon load balancing