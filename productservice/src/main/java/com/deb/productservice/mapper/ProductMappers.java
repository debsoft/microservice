package com.deb.productservice.mapper;

import java.util.function.Function;

import com.deb.productservice.dto.ProductDto;
import com.deb.productservice.model.Product;

public class ProductMappers {

	public static Function<ProductDto, Product> Product_DTO_TO_ENTITY = productDto -> {
		Product product = new Product();
		product.setId(productDto.getId());
		product.setProductName(productDto.getProductName());
		product.setDescription(productDto.getDescription());
		product.setPrice(productDto.getPrice());
		product.setBrand(BrandMappers.BRAND_DTO_TO_ENTITY.apply(productDto.getBrandDto()));
		product.setCategory(CategoryMappers.CATEGORY_DTO_TO_ENTITY.apply(productDto.getCategoryDto()));
		return product;
	};

	public static Function<Product, ProductDto> Product_ENTITY_TO_DTO = product -> {
		ProductDto productDto = new ProductDto();
		productDto.setId(product.getId());
		productDto.setProductName(product.getProductName());
		productDto.setDescription(product.getDescription());
		productDto.setPrice(product.getPrice());
		productDto.setBrandDto(BrandMappers.BRAND_ENTITY_TO_DTO.apply(product.getBrand()));
		productDto.setCategoryDto(CategoryMappers.CATEGORY_ENTITY_TO_DTO.apply(product.getCategory()));
		return productDto;
	};
}
