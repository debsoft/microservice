package com.deb.productservice.mapper;

import java.util.function.Function;

import com.deb.productservice.dto.CategoryDto;
import com.deb.productservice.dto.CategoryDto;
import com.deb.productservice.model.Category;
import com.deb.productservice.model.Category;

public class CategoryMappers {

	public static final Function<CategoryDto, Category> CATEGORY_DTO_TO_ENTITY = categoryDto -> {
		Category cCategory = new Category();
		cCategory.setId(categoryDto.getId());
		cCategory.setCategoryName(categoryDto.getCategoryName());
		cCategory.setDescription(categoryDto.getDescription());
		return cCategory;
	};

	public static final Function<Category, CategoryDto> CATEGORY_ENTITY_TO_DTO = category -> {
		CategoryDto categoryDto = new CategoryDto();
		categoryDto.setId(category.getId());
		categoryDto.setCategoryName(category.getCategoryName());
		categoryDto.setDescription(category.getDescription());
		return categoryDto;
	};

}
