package com.deb.productservice.mapper;

import java.util.function.Function;

import com.deb.productservice.dto.BrandDto;
import com.deb.productservice.model.Brand;

public class BrandMappers {

	public static Function<BrandDto, Brand> BRAND_DTO_TO_ENTITY = brandDto -> {
		Brand brand = new Brand();
		brand.setId(brandDto.getId());
		brand.setBrandName(brandDto.getBrandName());
		brand.setDescription(brandDto.getDescription());
		return brand;
	};

	public static Function<Brand, BrandDto> BRAND_ENTITY_TO_DTO = brand -> {
		BrandDto brandDto = new BrandDto();
		brandDto.setId(brand.getId());
		brandDto.setBrandName(brand.getBrandName());
		brandDto.setDescription(brand.getDescription());
		return brandDto;
	};
}
