package com.deb.productservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deb.productservice.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long> {

}
