package com.deb.productservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deb.productservice.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
