package com.deb.productservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deb.productservice.model.Brand;

public interface BrandRepository extends JpaRepository<Brand, Long> {

	public Brand findByBrandName(String brandName);
}
