package com.deb.productservice.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@EnableJpaAuditing
//@EnableTransactionManagement
@ComponentScan
@SpringBootApplication
@ComponentScan({"com.deb.productservice.*"})
@EntityScan({"com.deb.productservice.model"})
@EnableJpaRepositories({"com.deb.productservice.repo"})
@PropertySource("classpath:error_messages.properties")
public class ProductserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductserviceApplication.class, args);
	}

}
