package com.deb.productservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.deb.productservice.dto.BrandDto;
import com.deb.productservice.model.Brand;
import com.deb.productservice.service.BrandService;

@RestController
public class BrandController {

	@Autowired
	private BrandService brandService;
	
	@PostMapping("/brand")
	public ResponseEntity<Long> createBrand(@RequestBody BrandDto brandDto) {
		Long id =  brandService.saveBrand(brandDto);
		return null;
	}
}
