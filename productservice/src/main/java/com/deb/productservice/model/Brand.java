package com.deb.productservice.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "brand")
public class Brand extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "brand_name", length = 100, unique = true)
	private String brandName;
	@Column(name = "description", length = 2000)
	private String description;
	@OneToMany(mappedBy = "brand", fetch = FetchType.LAZY)
	private List<Product> productList;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	@Override
	public String toString() {
		return "Brand [brandName=" + brandName + ", description=" + description + ", productList=" + productList + "]";
	}

	public Brand() {
		super();
	}

}
