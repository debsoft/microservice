package com.deb.productservice.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "category")
public class Category extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "category_name", length = 100, unique = true)
	private String categoryName;
	@Column(name = "description", length = 2000)
	private String description;

	@OneToMany(mappedBy = "category", fetch = FetchType.LAZY)
	private List<Product> productList;

	public Category() {
		super();
	}

	@Override
	public String toString() {
		return "Category [categoryName=" + categoryName + ", description=" + description + ", productList="
				+ productList + "]";
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<Product> getProductList() {
		return productList;
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}
}
