package com.deb.productservice.service;

import java.util.List;

import com.deb.productservice.dto.BrandDto;
import com.deb.productservice.dto.ProductDto;

public interface BrandService {

	Long saveBrand(BrandDto brandDto);

	List<ProductDto> findProductsByBrand(String brandName);

}
