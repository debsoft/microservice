package com.deb.productservice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.deb.productservice.dto.BrandDto;
import com.deb.productservice.dto.ProductDto;
import com.deb.productservice.exception.UniqueConstraintsViolationException;
import com.deb.productservice.mapper.BrandMappers;
import com.deb.productservice.model.Brand;
import com.deb.productservice.repo.BrandRepository;
import com.deb.productservice.service.BrandService;

@Service
public class BrandServiceImpl implements BrandService {

	@Autowired
	private BrandRepository brandRepository;

	@Override
	public Long saveBrand(BrandDto brandDto) {
		if(brandDto != null && brandRepository.findByBrandName(brandDto.getBrandName()) != null) {
			throw new UniqueConstraintsViolationException("Brand already exists with Brand Name  " + brandDto.getBrandName());
		}
		Brand brand = brandRepository.save(BrandMappers.BRAND_DTO_TO_ENTITY.apply(brandDto));
		return brand.getId();
	}
	
	@Override
	public List<ProductDto> findProductsByBrand(String brandName) {
		return null;
	}
}
