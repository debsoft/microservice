package com.deb.userservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.deb.userservice.dto.UserDto;
import com.deb.userservice.mappers.UserMappers;
import com.deb.userservice.model.User;
import com.deb.userservice.service.UserService;

@RestController
public class UserController {

	
	@Autowired
	private UserService userService;
	
	@GetMapping("/user/list")
	public ResponseEntity getAllUsers() {
		return null;
	}
	
	@PostMapping("/user")
	public ResponseEntity createUser(@RequestBody UserDto userDto) {
		User user =userService.create(userDto);
		return new ResponseEntity(UserMappers.USER_ENTITY_TO_DTO.apply(user),HttpStatus.CREATED);
	}
}
