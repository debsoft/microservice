package com.deb.userservice.exception;

public class UserExistsException extends RuntimeException{
	
	public UserExistsException() {
		super();
	}
	
	public UserExistsException(String exceptionMsg) {
		super(exceptionMsg);
	}
}
