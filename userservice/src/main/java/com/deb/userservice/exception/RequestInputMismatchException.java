package com.deb.userservice.exception;

public class RequestInputMismatchException extends RuntimeException{
	
	public RequestInputMismatchException() {
		super();
	}
	
	public RequestInputMismatchException(String exceptionMsg) {
		super(exceptionMsg);
	}
}
