package com.deb.userservice.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.deb.userservice.dto.UserDto;
import com.deb.userservice.exception.UserExistsException;
import com.deb.userservice.mappers.UserMappers;
import com.deb.userservice.model.User;
import com.deb.userservice.repo.UserRepo;
import com.deb.userservice.service.UserService;

@Service
public class UserServiceImpl implements UserService{
	
	
	@Autowired
	private UserRepo userRepo;
	
	@Autowired
	private BCryptPasswordEncoder bcryptPasswordEncoder; 

	@Override
	public User getUserByName(String userName) {
		return userRepo.findByUserName(userName);
	}

	@Override
	public User getUserByEmail(String email) {
		return userRepo.findByEmail(email);
	}

	@Override
	public List<User> getAllUsers() {
		return userRepo.findAll();
	}

	@Override
	public User create(UserDto userDto) {
		if(userRepo.findByEmail(userDto.getEmail())!=null || userRepo.findByUserName(userDto.getUserName())!=null) {
			throw new UserExistsException();
		}
		userDto.setPassword(bcryptPasswordEncoder.encode(userDto.getPassword()));
		User user =UserMappers.USER_DTO_TO_ENTITY.apply(userDto);
		user =userRepo.save(user);
		return user;
	}

	@Override
	public User update(UserDto userDto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

}
