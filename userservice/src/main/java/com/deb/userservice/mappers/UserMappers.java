package com.deb.userservice.mappers;

import java.util.function.Function;

import com.deb.userservice.dto.UserDto;
import com.deb.userservice.model.User;

public class UserMappers {
	
	public static Function<User, UserDto> USER_ENTITY_TO_DTO = user -> {
	    UserDto dto = new UserDto();
	    if(user!=null) {
	    	dto.setId(user.getId());
	    	dto.setEmail(user.getEmail());
	    	dto.setFirstName(user.getFirstName());
	    	dto.setLastName(user.getLastName());
	    	dto.setMiddleName(user.getMiddleName());
	    	dto.setUserName(user.getUserName());
	    	
	    }
	    return dto;
	};
	public static Function<UserDto, User> USER_DTO_TO_ENTITY = userDto -> {
	    User user = new User();
	    user.setIsActive(true);
	    user.setEmail(userDto.getEmail());
	    user.setFirstName(userDto.getFirstName());
	    user.setMiddleName(userDto.getMiddleName());
	    user.setLastName(userDto.getLastName());
	    user.setUserName(userDto.getUserName());
	    user.setPassword(userDto.getPassword());
	    return user;
	};
}
